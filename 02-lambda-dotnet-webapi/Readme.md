# Create AWS lambda in localstack with AWS CLI

## AWS
Create empty sample C# lambda function from an Amazon template
```
dotnet new serverless.AspNetCoreWebAPI -n Sample.Lambda.DotNet.WebApi
```

Compile and publish
```
dotnet build
dotnet publish -c Release -o publish
```

Zip lambda files
```
cd publish
zip -r ../function.zip *
```

Create role
```
aws --profile diegosasw iam create-role --role-name lambda-dotnet-webapi-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
```

Attach AWSLambdaBasicExecutionRole policy to role
```
aws --profile diegosasw iam attach-role-policy --role-name lambda-dotnet-webapi-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Create lambda
```
aws --profile diegosasw lambda create-function --function-name lambda-dotnet-webapi-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet.WebApi::Sample.Lambda.DotNet.WebApi.LambdaEntryPoint::FunctionHandlerAsync --runtime dotnetcore3.1 --role arn:aws:iam::308309238958:role/lambda-dotnet-webapi-ex
```

Invoke lambda
```
aws --profile diegosasw lambda invoke --function-name lambda-dotnet-webapi-function --payload file://payload_sample.json response.json --log-type Tail
```

## LocalStack
Create empty sample C# lambda function from an Amazon template
```
dotnet new serverless.AspNetCoreWebAPI -n Sample.Lambda.DotNet.WebApi
```

Compile and publish
```
dotnet build
dotnet publish -c Release -o publish
```

Zip lambda files
```
cd publish
zip -r ../function.zip *
```

Create role
```
awslocal iam create-role --role-name lambda-dotnet-webapi-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
```

Attach AWSLambdaBasicExecutionRole policy to role
```
awslocal iam attach-role-policy --role-name lambda-dotnet-webapi-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Create lambda
```
awslocal lambda create-function --function-name lambda-dotnet-webapi-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet.WebApi::Sample.Lambda.DotNet.WebApi.LambdaEntryPoint::FunctionHandlerAsync --runtime dotnetcore3.1 --role arn:aws:iam::000000000000:role/lambda-dotnet-webapi-ex
```

Invoke lambda
```
awslocal lambda invoke --function-name lambda-dotnet-webapi-function --payload file://payload_sample.json response.json --log-type Tail
```

View functions
```
awslocal lambda list-functions
```

Delete function
```
awslocal lambda delete-function --function-name lambda-dotnet-function
```

## Dotnet tool
With dotnet tool, the equivalent is
```
dotnet lambda invoke-function lambda-dotnet-function --payload "Just Checking If Everything is OK" --profile diegosasw
```

For more information view https://github.com/aws/aws-extensions-for-dotnet-cli/blob/9639f8f4902349d289491b290979a3a1671cc0a5/src/Amazon.Lambda.Tools/Commands/InvokeFunctionCommand.cs#L84

## More info
`dotnet lambda` does not support LocalStack at the moment. See https://github.com/aws/aws-lambda-dotnet/issues/831












Create role for lambda execution
```
awslocal iam create-role --role-name lambda-dotnet-webapi-ex --assume-role-policy-document file://trust-policy.json
```

List all the roles and view it.
```
awslocal iam list-roles
```

If the role has an ARN `arn:aws:iam::000000000000:role/lambda-ex` you refer to the role as `lambda-ex`


Attach policy to the role to grant permission for execution
```
awslocal iam attach-role-policy --role-name lambda-dotnet-webapi-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Now create the AWS Lambda code with `index.js` and zip it as `function.zip`

Create Lambda function
```
awslocal lambda create-function --function-name lambda-dotnet-webapi-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet.WebApi::Sample.Lambda.DotNet.WebApi.LambdaEntryPoint::FunctionHandlerAsync --runtime dotnetcore3.1 --role arn:aws:iam::000000000000:role/lambda-dotnet-webapi-ex
```

Invoke the AWS Lambda using the base64 utility to decode the logs
```
awslocal lambda invoke --function-name lambda-dotnet-webapi-function out --log-type Tail --query 'LogResult' --output text | base64 -d
```

It returns:
```
iptables v1.4.21: can't initialize iptables table `nat': iptables who? (do you need to insmod?)
Perhaps iptables or your kernel needs to be upgraded.
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
[Information] Microsoft.Hosting.Lifetime: Application started. Press Ctrl+C to shut down.
[Information] Microsoft.Hosting.Lifetime: Hosting environment: Production
[Information] Microsoft.Hosting.Lifetime: Content root path: /var/task
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Production
info: Microsoft.Hosting.Lifetime[0]
      Content root path: /var/task
START RequestId: a5eb1d2d-d908-15f6-ace3-d4d0e01a0066 Version: $LATEST
Could not load file or assembly 'System.IO.Pipelines, Version=4.0.2.1, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'. The system cannot find the file specified.
: FileNotFoundException
   at Amazon.Lambda.AspNetCoreServer.AbstractAspNetCoreFunction`2.FunctionHandlerAsync(TREQUEST request, ILambdaContext lambdaContext)
   at System.Runtime.CompilerServices.AsyncMethodBuilderCore.Start[TStateMachine](TStateMachine& stateMachine)
   at Amazon.Lambda.AspNetCoreServer.AbstractAspNetCoreFunction`2.FunctionHandlerAsync(TREQUEST request, ILambdaContext lambdaContext)
   at lambda_method(Closure , Stream , Stream , LambdaContextInternal )


END RequestId: a5eb1d2d-d908-15f6-ace3-d4d0e01a0066
REPORT RequestId: a5eb1d2d-d908-15f6-ace3-d4d0e01a0066  Init Duration: 2305.87 ms       Duration: 33.29 ms      Billed Duration: 100 ms      Memory Size: 1536 MB    Max Memory Used: 0 MB
Starting daemons...
ImportError: No module named site
```


List the AWS Lambda functions in your account

```
awslocal lambda list-functions --max-items 10
```

To delete the AWS Lambda
```
awslocal lambda delete-function --function-name my-function
```

To delete the role
```
awslocal iam delete-role --role-name lambda-ex
```

More info: https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-awscli.html


## AWS
Create role
```
aws iam create-role --role-name lambda-dotnet-webapi-ex --assume-role-policy-document file://trust-policy.json --profile diegosasw
```
List roles
```
aws iam list-roles --profile diegosasw
```
Attach policy
```
aws iam attach-role-policy --role-name lambda-dotnet-webapi-ex
 --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole --profile diegosasw
```
Create lambda
```
aws lambda create-function --function-name lambda-dotnet-webap
i-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet.WebApi::Sample.Lambda.DotNet.WebApi.LambdaEntryPoint::FunctionHa
ndlerAsync --runtime dotnetcore3.1 --role arn:aws:iam::308309238958:role/lambda-dotnet-webapi-ex --profile diegosasw
```

Invoke
```
aws lambda invoke --function-name lambda-dotnet-webapi-function --profile diegosasw out --log-type Tail --query 'LogResult' --output text | base64 -d
```

It returns
```
START RequestId: 7d77489f-869b-4e4d-87a0-ac800d71eb2d Version: $LATEST
warn: Amazon.Lambda.AspNetCoreServer.AbstractAspNetCoreFunction[0]
      Request does not contain domain name information but is derived from APIGatewayProxyFunction.
[Warning] Amazon.Lambda.AspNetCoreServer.AbstractAspNetCoreFunction: Request does not contain domain name information but is derived from APIGatewayProxyFunction.
Object reference not set to an instance of an object.: NullReferenceException
   at Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction.MarshallRequest(InvokeFeatures features, APIGatewayProxyRequest apiGatewayRequest, ILambdaContext lambdaContext)
   at Amazon.Lambda.AspNetCoreServer.AbstractAspNetCoreFunction`2.FunctionHandlerAsync(TREQUEST request, ILambdaContext lambdaContext)
   at lambda_method(Closure , Stream , Stream , LambdaContextInternal )


END RequestId: 7d77489f-869b-4e4d-87a0-ac800d71eb2d
REPORT RequestId: 7d77489f-869b-4e4d-87a0-ac800d71eb2d  Duration: 755.06 ms     Billed Duration: 756 ms Memory Size: 128 MB     Max Memory Used: 87 MB    Init Duration: 462.09 ms
```