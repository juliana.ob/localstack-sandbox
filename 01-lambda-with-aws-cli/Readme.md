# Create AWS lambda in localstack with AWS CLI
Create role for lambda execution
```
awslocal iam create-role --role-name lambda-ex --assume-role-policy-document file://trust-policy.json
```

List all the roles and view it.
```
awslocal iam list-roles
```

If the role has an ARN `arn:aws:iam::000000000000:role/lambda-ex` you refer to the role as `lambda-ex`


Attach policy to the role to grant permission for execution
```
awslocal iam attach-role-policy --role-name lambda-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Now create the AWS Lambda code with `index.js` and zip it as `function.zip`

Create Lambda function
```
awslocal lambda create-function --function-name my-function --zip-file fileb://function.zip --handler index.handler --
runtime nodejs12.x --role arn:aws:iam::000000000000:role/lambda-ex
```

Invoke the AWS Lambda
```
awslocal lambda invoke --function-name my-function out --log-type Tail
```
Or better, use the base64 utility to decode the logs
```
awslocal lambda invoke --function-name my-function out --log-type Tail --query 'LogResult' --output text |base64 -d
```

List the AWS Lambda functions in your account

```
awslocal lambda list-functions --max-items 10
```

To delete the AWS Lambda
```
awslocal lambda delete-function --function-name my-function
```

To delete the role
```
awslocal iam delete-role --role-name lambda-ex
```

More info: https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-awscli.html