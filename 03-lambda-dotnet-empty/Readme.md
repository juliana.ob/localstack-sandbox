# Create AWS lambda in localstack with AWS CLI

## AWS
Create empty sample C# lambda function from an Amazon template
```
dotnet new lambda.EmptyFunction -n Sample.Lambda.DotNet
```

Compile and publish
```
dotnet build
dotnet publish -c Release -o publish
```

Zip lambda files
```
cd publish
zip -r ../function.zip *
```

Create role
```
aws --profile diegosasw iam create-role --role-name lambda-dotnet-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
```

Attach AWSLambdaBasicExecutionRole policy to role
```
aws --profile diegosasw iam attach-role-policy --role-name lambda-dotnet-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Create lambda
```
aws --profile diegosasw lambda create-function --function-name lambda-dotnet-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet::Sample.Lambda.DotNet.Function::FunctionHandler --runtime dotnetcore3.1 --role arn:aws:iam::308309238958:role/lambda-dotnet-ex
```

Invoke lambda
```
aws --profile diegosasw lambda invoke --function-name lambda-dotnet-function --payload "\"Just Checking If Everything is OK\"" response.json --log-type Tail
```

## LocalStack
Create empty sample C# lambda function from an Amazon template
```
dotnet new lambda.EmptyFunction -n Sample.Lambda.DotNet
```

Compile and publish
```
dotnet build
dotnet publish -c Release -o publish
```

Zip lambda files
```
cd publish
zip -r ../function.zip *
```

Create role
```
awslocal iam create-role --role-name lambda-dotnet-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
```

Attach AWSLambdaBasicExecutionRole policy to role
```
awslocal iam attach-role-policy --role-name lambda-dotnet-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Create lambda
```
awslocal lambda create-function --function-name lambda-dotnet-function --zip-file fileb://function.zip --handler Sample.Lambda.DotNet::Sample.Lambda.DotNet.Function::FunctionHandler --runtime dotnetcore3.1 --role arn:aws:iam::000000000000:role/lambda-dotnet-ex
```

Invoke lambda
```
awslocal lambda invoke --function-name lambda-dotnet-function --payload "\"Just Checking If Everything is OK again\"" response.json --log-type Tail
```

View functions
```
awslocal lambda list-functions
```

Delete function
```
awslocal lambda delete-function --function-name lambda-dotnet-function
```

## Dotnet tool
With dotnet tool, the equivalent is
```
dotnet lambda invoke-function lambda-dotnet-function --payload "Just Checking If Everything is OK" --profile diegosasw
```

For more information view https://github.com/aws/aws-extensions-for-dotnet-cli/blob/9639f8f4902349d289491b290979a3a1671cc0a5/src/Amazon.Lambda.Tools/Commands/InvokeFunctionCommand.cs#L84

## More info
`dotnet lambda` does not support LocalStack at the moment. See https://github.com/aws/aws-lambda-dotnet/issues/831